# ArmaniCrawler
Scrapy Crawler with Splash.
### Run
Create virtualenv

Install requirements

Install Docker:
```sh
$ curl -fsSL https://get.docker.com/ | sh
```
Pull Splash Container:
```sh
$ docker pull scrapinghub/splash
```
Run Splash Server via Docker:
```sh
$ docker run -p 5023:5023 -p 8050:8050 -p 8051:8051 scrapinghub/splash
```
Run Crawler:
```sh
$ cd armani
$ scrapy crawl armani_spider
```