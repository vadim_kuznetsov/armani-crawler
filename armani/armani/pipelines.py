# -*- coding: utf-8 -*-

from scrapy import signals
from scrapy.exporters import CsvItemExporter
import pandas as pd
import json


# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html


class ArmaniPipeline(object):
    def process_item(self, item, spider):
        return item


class CSVPipeline(object):
    def __init__(self):
        self.files = {}

    @classmethod
    def from_crawler(cls, crawler):
        pipeline = cls()
        crawler.signals.connect(pipeline.spider_opened, signals.spider_opened)
        crawler.signals.connect(pipeline.spider_closed, signals.spider_closed)
        return pipeline

    def spider_opened(self, spider):
        file = open('{}_items.csv'.format(spider.name), 'w+b')
        self.files[spider] = file
        self.exporter = CsvItemExporter(file)
        self.exporter.fields_to_export = ['name', 'price', 'currency', 'category', 'mfc', 'availability', 'time',
                                          'color', 'size', 'region', 'description']
        self.exporter.start_exporting()

    @staticmethod
    def save_test_results(test_results):
        json_data = {key: int(value) for key, value in test_results.items()}
        with open('test_results.json', 'w') as f:
            json.dump(json_data, f)

    @staticmethod
    def log_test_results(spider, test_results, currency_correctness):
        spider.log('TEST RESULTS')
        spider.log('Products in regions: USA  - {} France - {}'.format(test_results['count_products_us'],
                                                                       test_results['count_products_fr']))
        spider.log('Currency is correct: {}'.format(currency_correctness))
        spider.log('Filling: Color - {}% Size - {}% Description - {}%'.format(test_results['fill_color'],
                                                                              test_results['fill_size'],
                                                                              test_results['fill_desc']))

    def test_results(self, spider):
        df = pd.read_csv('{}_items.csv'.format(spider.name))
        products_in_regions = df.groupby('region').size()

        test_results = {'count_products_us': products_in_regions['US'],
                        'count_products_fr': products_in_regions['FR'],
                        'count_wrong_products_us': len(df[df.currency.isnull() & (df.region == 'US')].index),
                        'count_wrong_products_fr': len(df[df.currency.isnull() & (df.region == 'FR')].index),
                        'count_currency_usd': len(df[df.currency == 'USD'].index),
                        'count_currency_eur': len(df[df.currency == 'EUR'].index),
                        'fill_color': int(100 - (100 * df['color'].isnull().sum() / len(df.index))),
                        'fill_size': int(100 - (100 * df['size'].isnull().sum() / len(df.index))),
                        'fill_desc': int(100 - (100 * df['description'].isnull().sum() / len(df.index)))}

        count_not_correct_currency_us = test_results['count_products_us'] - test_results['count_currency_usd'] - \
                                        test_results['count_wrong_products_us']
        count_not_correct_currency_fr = test_results['count_products_fr'] - test_results['count_currency_eur'] - \
                                        test_results['count_wrong_products_fr']
        if count_not_correct_currency_us or count_not_correct_currency_fr:
            currency_correctness = 'NOT'
        else:
            currency_correctness = 'YES'

        self.log_test_results(spider, test_results, currency_correctness)
        self.save_test_results(test_results)

    def spider_closed(self, spider):
        self.exporter.finish_exporting()
        file = self.files.pop(spider)
        file.close()
        self.test_results(spider)

    def process_item(self, item, spider):
        self.exporter.export_item(item)
        return item
