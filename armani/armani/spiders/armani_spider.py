# -*- coding: utf-8 -*-

from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from ..items import ArmaniItem
import datetime


class ArmaniSpider(CrawlSpider):
    name = 'armani_spider'
    allowed_domains = ['www.armani.com']
    start_urls = [
        'http://www.armani.com/us/',
        'http://www.armani.com/fr/',
    ]

    rules = (
        Rule(LinkExtractor(
            allow=('http://www.armani.com/us/', 'http://www.armani.com/fr/',),
            deny=('.*_cod.*\.html',)),
            follow=True,
        ),
        Rule(LinkExtractor(
            allow=('.*_cod.*\.html',)),
            callback='parse_item',
            process_request='use_splash',
        )
    )

    @staticmethod
    def use_splash(request):
        request.meta['splash'] = {'endpoint': 'render.html', 'args': {'wait': 0.5}}
        return request

    def parse_item(self, response):
        self.logger.info('Product detected on: {}'.format(response.url.split('/')[-1]))
        item = ArmaniItem()
        item['name'] = response.xpath('//h1[@class="productName"]/text()').extract()
        item['price'] = response.xpath('//span[@class="priceValue"]/text()').extract_first()
        currency = response.xpath('//span[@class="currency"]/text()').extract_first()
        item['currency'] = currency.replace('$', 'USD') if currency else currency
        item['category'] = response.xpath('//li[@class="selected leaf"]/a/text()').extract()
        item['mfc'] = response.xpath('//span[@class="MFC"]/text()').extract()
        if response.xpath('//div[@class="soldOutButton"]/text()').extract() or \
                response.xpath('//span[@class="outStock"]/text()').extract():
            item['availability'] = 'No'
        else:
            item['availability'] = 'Yes'
        item['time'] = datetime.datetime.now().strftime("%H:%M %Y.%m.%d")
        item['color'] = response.xpath('//*[contains(@id, "color_")]/a/text()').extract()
        item['size'] = response.xpath('//*[contains(@id, "sizew_")]/a/text()').extract()
        item['region'] = 'US' if '/us/' in response.url else 'FR'
        description = response.xpath('//div[@class="descriptionContent"]/text()').extract_first()
        item['description'] = description if description != ' ' else None  # for pandas NaN
        return item
