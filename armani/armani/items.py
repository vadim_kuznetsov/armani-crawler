# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ArmaniItem(scrapy.Item):
    name = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    category = scrapy.Field()
    mfc = scrapy.Field()
    availability = scrapy.Field()
    time = scrapy.Field()
    color = scrapy.Field()
    size = scrapy.Field()
    region = scrapy.Field()
    description = scrapy.Field()
    pass
